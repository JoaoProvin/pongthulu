﻿using UnityEngine;
using System.Collections;

public class moveBarraDir : MonoBehaviour {

	public float velocidade = 1;
	public Vector3 posicBarra;

	void Update()
	{
		float yPos = transform.position.y;

		if (Input.GetKey ("up"))
			yPos = transform.position.y + velocidade;
		else if (Input.GetKey ("down"))
			yPos = transform.position.y - velocidade;
			

		posicBarra = new Vector3(150, Mathf.Clamp(yPos, -105, 105), 0);
		transform.position = posicBarra;
	}
}
