﻿using UnityEngine;
using System.Collections;

public class bola : MonoBehaviour
{
	public float velocidade = 6000;

	Rigidbody rb;
	bool ativo;
	int rand;

	void Awake()
	{
		rb = GetComponent<Rigidbody>();
	}

	void Update()
	{
		// Condição que inicia a partida.
		if (Input.GetKey("space") && ativo == false)
		{
			rand = Random.Range(1,3);
			ativo = true;
			rb.isKinematic = false;

			// Condição que utiliza o rand para determinar a direção inicial da bolinha.
			if (rand == 1)
				rb.AddForce(new Vector3(velocidade, velocidade, 0));
			else if (rand == 2)
				rb.AddForce(new Vector3(-velocidade, -velocidade, 0));
		}
	}

	void OnCollisionEnter (Collision col)
	{
		// Tentativa de aumentar velocidade da bolinha.
		if (col.gameObject.name == "BarraEsquerda")
			rb.AddForce(new Vector3 (750, 750, 0));
		else if (col.gameObject.name == "BarraDireita")
			rb.AddForce(new Vector3 (-750, -750, 0));

		// Condições para checar se a bolinha bateu nas paredes laterais, resultando em pontuação.
		if (col.gameObject.name == "ParedeEsquerda") {
			ativo = false;
			transform.position = new Vector3 (0, 0, 0);
			rb.isKinematic = true;	
		} else if (col.gameObject.name == "ParedeDireita") {
			ativo = false;
			transform.position = new Vector3 (0, 0, 0);
			rb.isKinematic = true;
		}
	}
}